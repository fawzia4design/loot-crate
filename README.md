# All Crate Design
+ The main html we will be working on is `crate.html` file
+ mainly we will be working inside `#all_crates_show` div in `crate.html` file so we don't want to change in `#all_crates_show` it-self but the html inside it
+ the all css in the page are coming from lootcrate pre-compiled css but I have included the `all_crates_manifest.scss` file which is exactly what the page require
+ In the `all_crates_manifest.scss` we have:
    *  `bootstrap`, `font-awesome`, other third-party libs
    *  `base`, `layout`, `modules` css the we can't change
    *  `cms/all_crates` the only file we can change
+  Idealy we will work in `cms/all_crates` under `all-crates` class
+  For the javascript, all javascript are also compiled and come from lootcrate pre-compiled js but all javascript libs names are in `tmp/application.js` so you can assume the site already has
    *  `jquery`
    *  `jquery_uj`
    *  `select2`
    *  `bootstrap`

## Questions/Notes
+ can you compile scss locally to make `crate.html` run locally on `all_crates_manifest.scss` so you can edit `cms/all_crates` to reflect the new changes ?
+ Idealy you can create a repo in blink22 bitbucket to track design changes
+ most Images are comming from their CDN and won't show because of `Access-Control` problems, so if you would include any tmp images or place holders from the original site or zeplin to require them localy will be grateful

# Manage Account Design
+ the main html file we will work on is `user_accounts.html`
+ we will be working in `#lc_user_accounts` div in that file
+ all css are comming from `application_user_accounts.scss`
+ idealy the only css edits will be in `layout/user_accounts` folder, feel free to add new file as needed to be able to maintain that css code
+ be careful when working under `layout/user_account` files, because we aren't free edit/remove those file, so idealy we will create our new files and use different class names if possible, i think we could start by placing our css in `user_account.scss` file
+ all css for that page should be under `.user_accounts` class in any file

## TODO
I have commended the old `.main-content` and added your html `.main-content` inside `#lc_user_accounts` div

1- move manage account css from `all_crates` to `layout/user_accounts` folder inside `user_accounts.scss`